package com.example.junit;

public class MyClass {

    int x;

    public MyClass() {
        x=5;
    };

    public int multiply(int x, int y) {
        return x * y;
    }

    public int add(int firstNum, int secondNum) {
        return firstNum + secondNum;
    }

    public int divide(int a, int b) {
        return a / b;
    }

    public int subtract(int num1, int num2) {
        return num1 - num2;
    }
}
