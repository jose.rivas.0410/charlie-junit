package com.example.junit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JunitApplication {

	public static void main(String[] args) {
		SpringApplication.run(JunitApplication.class, args);

		MyClass test = new MyClass();
		System.out.println(test.x);

		System.out.println(test.divide(10, 2));

	}

}
