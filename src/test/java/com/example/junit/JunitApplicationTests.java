package com.example.junit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class JunitApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	public void multiplicationOfZeroIntegersShouldReturnZero() {
		MyClass tester = new MyClass();

		// assert statements
		// assertEquals(expected, actual);
		assertEquals(0, tester.multiply(10, 0), "10 * 0 must be 0");
		assertEquals(0, tester.multiply(0, 10), "0 * 10 must be 0");
		assertEquals(0, tester.multiply(0, 0), "0 * 0 should return 0");
	}

	@Test
	public void addTwoPlusTwoReturnFour() {
		MyClass tester2 = new MyClass();
		// arrange
		final int expected = 4;

		// actual
		final int actual = tester2.add(2, 2);

		assertEquals(expected, actual);
	}

	@Test
	public void divideByTenReturnFive() {
		MyClass tester3 = new MyClass();
		final int expected = 5;
		final int actual = tester3.divide(10, 2);
		assertEquals(expected, actual);
	}

}
